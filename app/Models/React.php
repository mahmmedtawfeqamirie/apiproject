<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class React extends Model
{
    use HasFactory;

    protected $table = 'reacts';

    protected $fillable = [
        'userId',
        'postId',
        'react'
    ];

    public function createReact($data)
    {
        return self::create($data);
    }

    public function updateReact($data)
    {
        $react = self::find($data['reactId']);
        $react->react = $data['react'];
        $react->save();
        return $react;
    }

    public function undoReact($data)
    {
        $record = self::where('userId', $data['userId'])->where('postId', $data['postId'])->first();

        if ($record != null) {

            $react = $data['react'];
            if ($record->react != 'none')
                $react = 'none';

            $record->react = $react;
            $record->save();
        }
        return $record;
    }


}
