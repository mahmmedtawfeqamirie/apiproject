<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable, HasApiTokens;

    protected $fillable = [
        'name',
        'email',
        'password',
        'role'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($value)
    {
        return $this->attributes['password'] = Hash::make($value);
    }

    public function createUser($data)
    {
        return self::create($data);
    }

    public function login($credentials)
    {
        $user = null;
        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            $user->token_api = $user->createToken('facebook')->accessToken;
        }
        return $user;
    }

    public function getUsersExepctAuth()
    {
        return self::select('id')->where('id', '<>', Auth::id())->get();
    }
}
