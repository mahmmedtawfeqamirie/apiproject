<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "posts";

    protected $fillable = [
        'description',
        'ImagePath',
        'userId'
    ];

    public function createPost($data)
    {
        return self::create($data);
    }

    public function getPostById($Id)
    {
        return self::find($Id);
    }

    public function updatePost($post, $data)
    {
        $post->description = $data['description'];
        $post->ImagePath = $data['ImagePath'];
        $post->save();
        return $post;
    }

    public function deletePost($post)
    {
        $post->delete();
        return $post;
    }

}
