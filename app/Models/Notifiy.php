<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notifiy extends Model
{
    use HasFactory;

    protected $table = 'notifications';

    protected $fillable = [
        'type',
        'ownerId',
        'postId',
        'commentId',
        'userId'
    ];

    public function createNotifiy($data)
    {
        return self::insert($data);
    }

}
