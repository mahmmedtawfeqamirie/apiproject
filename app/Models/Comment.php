<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'comments';

    protected $fillable = [
        'comment',
        'userId',
        'postId',
        'replyId'
    ];


    public function createComment($data)
    {
        return self::create($data);
    }

    public function getCommentById($Id)
    {
        return self::find($Id);
    }

    public function updateComment($comment, $data)
    {
        $comment->comment = $data['comment'];
        $comment->save();
        return $comment;
    }

    public function deleteComment($comment)
    {
        $comment->delete();
        return $comment;
    }

}
