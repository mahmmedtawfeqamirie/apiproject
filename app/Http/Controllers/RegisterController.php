<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use App\Models\User;

class RegisterController extends Controller
{

    protected $users;

    public function __construct(User $user)
    {
        $this->users = $user;
        $this->middleware('guest');
    }

    public function create(RegisterRequest $request)
    {
        $data = $request->only(['name', 'email', 'password']);
        $data['role'] = 'standard';

        $user = $this->users->createUser($data);

        return response()->json(['user' => $user], 200);
    }
}
