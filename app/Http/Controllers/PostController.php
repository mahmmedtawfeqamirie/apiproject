<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePostRequest;
use App\Http\Requests\DeletePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Models\Post;
use App\Models\Notifiy;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    protected $posts, $notifiy, $user;

    public function __construct(Post $posts, Notifiy $notifiy, User $user)
    {
        $this->posts = $posts;
        $this->notifiy = $notifiy;
        $this->user = $user;
    }

    public function create(CreatePostRequest $request)
    {
        $data['description'] = $request->get('description');
        $data['ImagePath'] = $request->get('ImagePath');
        $data['userId'] = Auth::id();
        $post = $this->posts->createPost($data);

        $users = $this->user->getUsersExepctAuth();
        $dataset = [];

        foreach ($users as $user)
            array_push($dataset, [
                'type' => 'post',
                'postId' => $post->id,
                'ownerId' => Auth::id(),
                'userId' => $user->id,
                'created_at' => Carbon::now(),
                'updated_at' => now()]);

        $this->notifiy->createNotifiy($dataset);

        return response()->json(['post' => $post, 'notifies' => $dataset], 200);
    }

    public function update(UpdatePostRequest $request)
    {
        $data = $request->only('postId', 'description');
        $data['ImagePath'] = $request->get('ImagePath');
        $data['userId'] = Auth::id();

        $postRecord = $this->posts->getPostById($data['postId']);
        $this->authorize('update', $postRecord);
        $post = $this->posts->updatePost($postRecord, $data);
        return response()->json(['post' => $post], 200);
    }

    public function delete(DeletePostRequest $request)
    {
        $Id = $request->get('postId');
        $postRecord = $this->posts->getPostById($Id);

        $this->authorize('delete', $postRecord);

        $post = $this->posts->deletePost($postRecord);

        if ($post != null)
            return response()->json(['post' => $post], 200);

        return response()->json(['error' => "Access Denied"], 400);
    }
}
