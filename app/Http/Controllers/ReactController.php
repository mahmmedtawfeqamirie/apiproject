<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateReactRequest;
use App\Http\Requests\UpdateReactRequest;
use App\Models\React;
use Illuminate\Support\Facades\Auth;

class ReactController extends Controller
{
    private $reacts;

    public function __construct(React $reacts)
    {
        $this->reacts = $reacts;
    }

    public function create(CreateReactRequest $request)
    {
        $data = $request->only('postId', 'react');
        $data['userId'] = Auth::id();

        $react = $this->reacts->undoReact($data);

        if ($react == null)
            $react = $this->reacts->createReact($data);

        return response()->json(['react' => $react], 200);
    }

    public function update(UpdateReactRequest $request)
    {
        $data = $request->only('react', 'reactId');
        $react = $this->reacts->updateReact($data);

        return response()->json(['react' => $react], 200);
    }
}
