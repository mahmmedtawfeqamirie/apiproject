<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCommentRequest;
use App\Http\Requests\DeleteCommentRequest;
use App\Http\Requests\UpdateCommentRequest;
use App\Models\Comment;
use App\Models\Notifiy;
use App\Models\Post;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    protected $comments, $users, $notifiy, $posts;


    public function __construct(Comment $comments, User $users, Notifiy $notifiy, Post $posts)
    {
        $this->comments = $comments;
        $this->users = $users;
        $this->notifiy = $notifiy;
        $this->posts = $posts;
    }

    public function create(CreateCommentRequest $request)
    {
        $data = $request->only('comment', 'postId');
        $data['replyId'] = $request->get('replyId');
        $data['userId'] = Auth::id();

        $comment = $this->comments->createComment($data);

        $oldComment = $this->comments->getCommentById($data['replyId']);
        $post = $this->posts->getPostById($data['postId']);

        $dataSet = [];
        array_push($dataSet, [
            'type' => 'comment',
            'commentId' => $comment->id,
            'ownerId' => Auth::id(),
            'userId' => $post->userId,
            'created_at' => Carbon::now(),
            'updated_at' => now()]);
        if ($data['replyId'] != null && Auth::id() != $oldComment->userId) {
            array_push($dataSet, [
                'type' => 'reply',
                'commentId' => $comment->id,
                'ownerId' => Auth::id(),
                'userId' => $oldComment->userId,
                'created_at' => Carbon::now(),
                'updated_at' => now()]);
        }

        $this->notifiy->createNotifiy($dataSet);

        return response()->json(['comment' => $comment, 'notifications' => $dataSet], 200);

    }

    public function update(UpdateCommentRequest $request)
    {
        $data = $request->only('comment', 'commentId');
        $commentRecord = $this->comments->getCommentById($data['commentId']);

        $this->authorize('update', $commentRecord);

        $comment = $this->comments->updateComment($commentRecord, $data);
        return response()->json(['comment' => $comment], 200);
    }

    public function delete(DeleteCommentRequest $request)
    {
        $Id = $request->get('commentId');
        $commentRecord = $this->comments->getCommentById($Id);

        $this->authorize('delete', $commentRecord);

        $comment = $this->comments->deleteComment($commentRecord);
        return response()->json(['comment' => $comment], 200);
    }

}
