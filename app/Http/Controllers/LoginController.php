<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{

    protected $users;

    public function __construct(User $users)
    {
        $this->users = $users;
    }

    public function login(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');
        $user = $this->users->login($credentials);

        if ($user != null)
            return response()->json(['user' => $user], 200);

        return response()->json(["error" => "wrong email or password try again"], 404);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
    }
}
