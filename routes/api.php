<?php

use App\Http\Controllers\CommentController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ReactController;
use App\Http\Controllers\RegisterController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::middleware('auth:api')->get('user', function (Request $request) {
    return $request->user();
});

//===========================================
//        Register Controller
//===========================================

Route::post('user/create', [RegisterController::class, 'create']);

//===========================================
//        Login Controller
//===========================================

Route::post('login', [LoginController::class, 'login']);

Route::middleware('auth:api')->group(function () {
    Route::get('logout', [LoginController::class, 'logout']);
});

//===========================================
//        Post Controller
//===========================================

Route::middleware('auth:api')->prefix('post')->group(function () {
    Route::post('create', [PostController::class, 'create']);
    Route::post('update', [PostController::class, 'update']);
    Route::post('delete', [PostController::class, 'delete']);
});

//===========================================
//        React Controller
//===========================================

Route::middleware('auth:api')->prefix('react')->group(function () {
    Route::post('create', [ReactController::class, 'create']);
    Route::post('update', [ReactController::class, 'update']);
});

//===========================================
//        Comment Controller
//===========================================

Route::middleware('auth:api')->prefix('comment')->group(function () {
    Route::post('create', [CommentController::class, 'create']);
    Route::post('update', [CommentController::class, 'update']);
    Route::post('delete', [CommentController::class, 'delete']);
});

