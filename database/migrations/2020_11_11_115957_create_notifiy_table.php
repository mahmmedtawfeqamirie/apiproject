<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotifiyTable extends Migration
{

    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->id();
            $table->enum('type', ['post', 'comment', 'reply']);
            $table->unsignedBigInteger('ownerId')->nullable();
            $table->foreign('ownerId')->references('id')->on('users');
            $table->unsignedBigInteger('postId')->nullable();
            $table->foreign('postId')->references('id')->on('posts');
            $table->unsignedBigInteger('commentId')->nullable();
            $table->foreign('commentId')->references('id')->on('comments');
            $table->unsignedBigInteger('userId')->nullable();
            $table->foreign('userId')->references('id')->on('users');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
